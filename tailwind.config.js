const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  content: ['./app/**/*.{html,js,jsx}'],
  theme: {
    // debugScreens: {
    //   position: ['top', 'left'],
    // },
    // fontFamily: {
    //   display: ['Chakrapetch'],
    //   body: ['Chakrapetch'],
    // },
    screens: {
      xs: '320px',
      ...defaultTheme.screens,
    },
    extend: {
      animation: {
        'spin-slow': 'spin 5s linear infinite',
      },
      fontFamily: {
        ubuntu: "'Ubuntu Mono'",
      },
      fontSize: {
        xxs: ['0.75rem', { lineHeight: '.75rem' }],
      },
      screens: {
        xs: '320px',
      },
      width: {
        0.5: '0.12rem',
        128: '32rem',
      },
      height: {
        0.5: '0.12rem',
        150: '30rem',
        180: '35rem',
      },
      minHeight: {
        150: '30rem',
        180: '35rem',
      },
      colors: {
        psyOrange: '#FBB16D',
        psyLightPink: '#F49DC7',
        psyDarkPink: '#F06BA7',
        psyCyan: '#71FFDE',
        psyBlue: '#06C3F6',
        psyLighterPink: '#E7669F',
      },
    },
  },
  plugins: [require('tailwindcss-debug-screens')],
};
