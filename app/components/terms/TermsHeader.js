import React from 'react';
import PropTypes from 'prop-types';

const TermsHeader = ({ title }) => {
  return (
    <div className='flex flex-col pt-3 pb-1'>
      <h2 className='font-bold text-2xl text-psyOrange'>{title}</h2>
    </div>
  );
};

TermsHeader.propTypes = {
  title: PropTypes.string.isRequired,
};

export default TermsHeader;
