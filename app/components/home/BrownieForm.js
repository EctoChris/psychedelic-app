import { useState } from 'react';
import ReCAPTCHA from 'react-google-recaptcha';
import { useFormik } from 'formik';
import FormikTextInput from '../shared/FormikTextInput';
import BrownieFormValidation from './BrownieFormValidation';
import { Form, useActionData, useTransition } from 'remix';
import { RingLoader } from 'react-spinners';
import FormikCheckbox from '../shared/FormikCheckbox';
import { Link } from 'remix';

const BrownieForm = () => {
  const data = useActionData();
  const transition = useTransition();
  const [submissionAttempt, setSubmissionAttempt] = useState(false);
  const [token, setToken] = useState(null);

  // Callback function to handle reCAPTCHA verification
  const handleRecaptchaVerify = (token) => {
    setToken(token);
  };

  const formik = useFormik({
    initialValues: { name: '', email: '', emailReType: '', agreement: false },
    validateOnChange: true,
    validationSchema: BrownieFormValidation,
    onSubmit: (values) => {
      if (!token) return;
    },
  });

  function handleChange(event) {
    setSubmissionAttempt(true);
    if (!token) {
      event.preventDefault();
      return false;
    }
    formik.validateField();
    if (!formik.isValid) event.preventDefault();
    formik.submitForm();
    return false;
  }

  return (
    <Form method='POST' className='mx-auto my-2' onSubmit={handleChange}>
      <FormikTextInput name='name' placeholder='First name' formik={formik} submissionAttempt={submissionAttempt} />
      <FormikTextInput name='email' placeholder='Email' formik={formik} submissionAttempt={submissionAttempt} />
      <FormikTextInput
        name='emailReType'
        placeholder='Re-type Email'
        formik={formik}
        submissionAttempt={submissionAttempt}
      />
      <input name='token' value={token || undefined} hidden={true} />
      <FormikCheckbox
        name='agreement'
        placeholder={
          <span>
            I agree the Website{' '}
            <Link to='/terms' className='text-blue-700'>
              Terms of Use
            </Link>{' '}
            and{' '}
            <Link to='/privacy' className='text-blue-700'>
              Privacy Policy
            </Link>{' '}
            & agree to receive future book promotions
          </span>
        }
        formik={formik}
        submissionAttempt={submissionAttempt}
      />
      <ReCAPTCHA sitekey='6LeEIl0lAAAAACzrQg2kjfRPclJJr45rCqBe7SLq' onChange={handleRecaptchaVerify} />
      {/* <Recaptcha captchaRef={ref} setCaptchaToken={setCaptchaRef} captchaError={false} /> */}
      <button
        className='w-full p-2 my-2 text-black transition-all duration-300 rounded-lg bg-psyBlue hover:bg-psyCyan'
        onClick={() => {
          setSubmissionAttempt(true);
          formik.validateField();
          formik.submitForm();
        }}
        type='submit'
      >
        {transition.state === 'submitting' ? <RingLoader size={25} /> : `Submit`}
      </button>
      <div className='relative w-72 centered'>
        <span className='w-72 text-sm'>{data?.message}</span>
      </div>
    </Form>
  );
};

export default BrownieForm;

// Loader runs on server: get data / perform action on server and use hook to access on client
// export const loader = () => {
//   const dummyData = {
//     posts: [
//       { title: 'post one', id: '1', body: 'this is a test post' },
//       { title: 'post two', id: '2', body: 'this is a test post' },
//       { title: 'post three ', id: '3', body: 'this is a test post' },
//       { title: 'testPost', id: '4', body: 'this is a test post' },
//     ],
//   };
//   console.log('123');
//   return dummyData;
// };
