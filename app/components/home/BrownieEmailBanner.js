import React from 'react';
import weedBrownies from '../../assets/home/freeBrownies.png';
import BrownieForm from './BrownieForm';

const BrownieEmailBanner = () => {
  return (
    <div className='flex-col justify-center w-full text-center bg-psyOrange'>
      <div className='pt-5'>
        <span className='text-lg'>Simply enter your email and receive the recipe!</span>
      </div>
      <div className='flex flex-col justify-around w-full py-3 mx-auto lg:w-8/12 sm:w-10/12 md:flex-row '>
        <div className='h-80 xs:h-56 sm:h-72'>
          <img src={weedBrownies} alt='weed brownies' className='h-full mx-auto' />
        </div>
        <BrownieForm />
      </div>
    </div>
  );
};

export default BrownieEmailBanner;
