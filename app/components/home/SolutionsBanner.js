import React from 'react';
import { Link } from 'remix';

const SolutionsBanner = () => {
  return (
    <div className='w-full bg-transparent centered h-80'>
      <Link
        to='solutions'
        className='px-10 py-5 my-auto text-xl transition-all duration-300 rounded-lg bg-gradient-to-r from-purple-500 to-pink-500 text-white hover:text-psyBlue border-2 border-white  hover:border-psyBlue'
      >
        View Solutions
      </Link>
    </div>
  );
};

export default SolutionsBanner;
