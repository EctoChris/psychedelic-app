/** @jsx jsx */
import { jsx, css } from '@emotion/react';
import { useEffect, useMemo, useState } from 'react';
import { useMousePos } from '~/hooks/useMousePos';
import { useScreenDimensions } from '~/hooks/useScreenDimensions';

import psyStar from '../../assets/home/psyStar.png';

const calculateMiddleScreen = (x, y) => {
  const midX = x / 2;
  const midY = y / 2;
  return { x: midX, y: midY };
};

const TitleBanner = () => {
  const [mouseOffsets, setMouseOffsets] = useState({ offsetX: 0, offsetY: 0 });
  const { width: screenWidth, height: screenHeight } = useScreenDimensions();
  const { x, y } = useMousePos();

  const midScreen = useMemo(() => calculateMiddleScreen(screenWidth, screenHeight), [screenWidth, screenHeight]);

  useEffect(() => {
    let offsetX = Math.abs(midScreen.x - x);
    let offsetY = Math.abs(midScreen.y - y);
    setMouseOffsets({ offsetX, offsetY });
  }, [x, y]);

  const classes = useStyles({
    offsetX: parseInt(mouseOffsets.offsetX * 0.05),
    offsetY: parseInt(mouseOffsets.offsetY * 0.05),
  });
  return (
    <div className='relative w-1/2 p-3 centered-text'>
      <div className='absolute z-20 p-3 w-2/2 bg-psyCyan bg-opacity-80 centered-text'>
        <h1>The Psychedelic Hidden Picture Book</h1>
      </div>
      <img css={classes.starOne} src={psyStar} alt='psychedelic star' />
      <img css={classes.starTwo} src={psyStar} alt='psychedelic star' />
      <img css={classes.starThree} src={psyStar} alt='psychedelic star' />
    </div>
  );
};

const useStyles = ({ offsetX, offsetY }) => {
  return {
    starOne: css`
      width: 80px;
      position: absolute;
      transform-style: preserve-3d;
      top: -30px;
      z-index: 40;
      transform: translate3d(${offsetX}px, -${offsetY}px, 0) rotate(-${offsetX}deg);
    `,
    starTwo: css`
      width: 120px;
      position: absolute;
      left: -80px;
      transform: translate3d(-${offsetX}px, 0, 0) rotate(${offsetY}deg);
    `,
    starThree: css`
      width: 140px;
      position: absolute;
      top: 90px;
      transform: translate3d(${offsetX}px, ${offsetY}px, 0) rotate(${offsetX}deg);
    `,
  };
};

export default TitleBanner;
