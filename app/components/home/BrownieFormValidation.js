import * as yup from 'yup';

const BrownieFormValidation = () => {
  return yup.object({
    name: yup.string().required('Please enter your first name'),
    agreement: yup.bool().required('ddd').oneOf([true], 'You must agree to the above statement'),
    email: yup
      .string()
      .email('Insert a valid email')
      .required('Your email is required')
      .test(
        'emailCheck',
        'Invalid email, your email must not include special characters',
        (value) => value !== undefined && value.length > 1
      ),
    emailReType: yup
      .string()
      .required('Please retype your email')
      .email('Insert a valid email')
      .test({
        name: 'max',
        exclusive: false,
        params: {},
        message: 'Entered emails must match',
        test: function (value) {
          return value === this.parent.email;
        },
      }),
  });
};

export default BrownieFormValidation;
