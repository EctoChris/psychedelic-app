import { Link } from 'remix';
import sunAndMoon from '../../assets/solutions/SunAndMoon.png';
import useQuery from '../../hooks/useQuery';

const Navbar = () => {
  const { show_solutions } = useQuery(['show_solutions']);
  return (
    <div className='bg-psyOrange p-2 flex items-center'>
      <Link to='/'>
        <div className='w-10 h-10 overflow-hidden rounded-full bg-gray-800'>
          <img src={sunAndMoon} alt='Psychedelic logo' className='w-12/12' />
        </div>
      </Link>
      {/* <Link to='/about' class='xs:px-1 sm:px-2 md:px-4'>
        About
      </Link> */}
      <Link to='/solutions' className='xs:px-2 sm:px-2 md:px-4'>
        Solutions
      </Link>
      <Link to='/terms' className='xs:px-2 sm:px-2 md:px-4'>
        Terms & Conditions
      </Link>
      <Link to='/privacy' className='xs:px-2 sm:px-2 md:px-4'>
        Privacy Policy
      </Link>
    </div>
  );
};

export default Navbar;
