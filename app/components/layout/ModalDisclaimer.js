/** @jsx jsx */
import { jsx, css } from '@emotion/react';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

const SEEN_DISCLAIMER = 'seenDisclaimer';

const ModalDisclaimer = () => {
  const [showModal, setShowModal] = useState(false);
  const classes = useStyles({ showModal });

  useEffect(() => {
    const seenDiclaimer = localStorage.getItem(SEEN_DISCLAIMER);
    if (!seenDiclaimer) setShowModal(true);
  }, []);

  const closeHandler = () => {
    localStorage.setItem(SEEN_DISCLAIMER, true);
    setShowModal(false);
  };

  return (
    <div css={classes.modalContainer}>
      <div css={classes.container}>
        <p className='font-sans p-2 xs:mx-1 sm:mx-4 md:mx-8 lg:mx-16'>
          Please note that our site contains content about marijuana/cannabis and other related substances that are
          illegal under federal law and may well be illegal under state law in certain states. All information and
          content on the site, services or downloadable material is for informational purposes only. The website does
          not offer medical advice, any information accessed through the site is not intended to be a substitute for
          medical advice. You are required to check your governing laws before accessing or using this website.
        </p>
        <button
          className='w-full p-2 my-1 text-black  rounded-lg bg-psyBlue hover:bg-psyCyan'
          onClick={closeHandler}
          type='submit'
        >
          Confirm
        </button>
        <p className='mx-auto'>
          &copy; 2022. psypublishing.com. All Rights Reserved. Use of this Site is subject to certain&nbsp;
          <Link to='/terms' className='text-blue-800 underline'>
            Terms of Use
          </Link>
        </p>
      </div>
      ;
    </div>
  );
};

const useStyles = ({ showModal }) => {
  const visibility = showModal ? 'visible' : 'hidden';
  return {
    modalContainer: css`
      visibility: ${visibility};
      background-color: rgba(0, 0, 0, 30%);
      z-index: 99;
      position: fixed;
      cursor: normal;
      top: 0;
      left: 0;
      width: 100%;
      min-width: 100%;
      height: 100%;
      display: flex;
      justify-content: center;
      align-items: flex-end;
    `,
    container: css`
      width: 100%;
      display: flex;
      flex-direction: column;
      background-color: white;
      padding: 0.5rem;
    `,
  };
};

export default ModalDisclaimer;
