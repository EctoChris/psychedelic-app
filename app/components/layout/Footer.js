import { Link } from 'react-router-dom';
import moment from 'moment';

const Footer = () => {
  return (
    <div className='bg-psyBlue p-2 flex flex-col items-center font-sans py-4 text-center xs:text-xs sm:text-sm'>
      <p className='mx-5 py-2'>
        Please note that our site contains content about marijuana/cannabis and other related substances that are
        illegal under federal law and may well be illegal under state law in certain states. All information and content
        on the site, services or downloadable material is for informational purposes only. The website does not offer
        medical advice, any information accessed through the site is not intended to be a substitute for medical advice.
        You are required to check your governing laws before accessing or using this website.
      </p>
      <p>
        &copy; {moment().year()}. psypublishing.com. All Rights Reserved. Use of this Site is subject to certain&nbsp;
        <Link to='/terms' className='text-blue-800 underline'>
          Terms of Use
        </Link>
      </p>
    </div>
  );
};

export default Footer;
