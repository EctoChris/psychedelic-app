import SunAndMoon from '../../assets/solutions/loadingSunAndMoon.svg';
import { useEffect, useState } from 'react';

const Loader = (loading) => {
  const [currentDots, setCurrentDots] = useState('.');

  function adjustLoadingMessage() {
    if (currentDots === '.') {
      setCurrentDots('..');
    } else if (currentDots.length === 2) {
      setCurrentDots('.');
    }
  }

  useEffect(() => {
    const id = setInterval(() => adjustLoadingMessage(), 1000);
    return () => clearInterval(id);
  }, [currentDots]);

  if (loading)
    return (
      <div>
        <img src={SunAndMoon} alt='sun and moon hugging' className='animate-spin-slow w-8/12 mx-auto' />
        <h3 className='font-miltonian'>Loading {currentDots}</h3>
        <h3>Loading</h3>
      </div>
    );
};

export default Loader;
