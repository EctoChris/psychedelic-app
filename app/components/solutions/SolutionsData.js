import artwork01Solution from '../../assets/solutions/compressjpgArtwork01Solution.jpg';
import artwork02Solution from '../../assets/solutions/compressjpgArtwork02Solution.jpg';
import artwork03Solution from '../../assets/solutions/compressjpgArtwork03Solution.jpg';
import artwork04Solution from '../../assets/solutions/compressjpgArtwork04Solution.jpg';
import artwork05Solution from '../../assets/solutions/compressjpgArtwork05Solution.jpg';
import artwork06Solution from '../../assets/solutions/compressjpgArtwork06Solution.jpg';
import artwork07Solution from '../../assets/solutions/compressjpgArtwork07Solution.jpg';
import artwork08Solution from '../../assets/solutions/compressjpgArtwork08Solution.jpg';
import artwork09Solution from '../../assets/solutions/compressjpgArtwork09Solution.jpg';
import artwork10Solution from '../../assets/solutions/compressjpgArtwork10Solution.jpg';
import artwork11Solution from '../../assets/solutions/compressjpgArtwork11Solution.jpg';
import artwork12Solution from '../../assets/solutions/compressjpgArtwork12Solution.jpg';
import artwork13Solution from '../../assets/solutions/compressjpgArtwork13Solution.jpg';
import artwork14Solution from '../../assets/solutions/compressjpgArtwork14Solution.jpg';
import artwork15Solution from '../../assets/solutions/compressjpgArtwork15Solution.jpg';
import artwork16Solution from '../../assets/solutions/compressjpgArtwork16Solution.jpg';
import artwork17Solution from '../../assets/solutions/compressjpgArtwork17Solution.jpg';
import artwork18Solution from '../../assets/solutions/compressjpgArtwork18Solution.jpg';
import artwork19Solution from '../../assets/solutions/compressjpgArtwork19Solution.jpg';
import artwork20Solution from '../../assets/solutions/compressjpgArtwork20Solution.jpg';
import artwork21Solution from '../../assets/solutions/compressjpgArtwork21Solution.jpg';
import artwork22Solution from '../../assets/solutions/compressjpgArtwork22Solution.jpg';

export const solutionsArray = [
  {
    img: artwork01Solution,
    imgSrc: '../../assets/solutions/compressjpgArtwork01Solution.jpeg',
    pageNo: 1,
    title: 'Alien Planet',
  },
  {
    img: artwork02Solution,
    imgSrc: '../../assets/solutions/compressjpgArtwork01Solution.jpeg',
    pageNo: 2,
    title: 'Psychedelic Mushroom Forets',
  },
  {
    img: artwork03Solution,
    pageNo: 3,
    title: 'Weed Town',
  },
  {
    img: artwork04Solution,
    pageNo: 4,
    title: 'Green Alien Planet',
  },
  {
    img: artwork05Solution,
    pageNo: 5,
    title: 'Hell vs Heaven',
  },
  {
    img: artwork06Solution,
    pageNo: 6,
    title: 'A Bad Trip',
  },
  {
    img: artwork07Solution,
    pageNo: 7,
    title: 'San Pedro Desert',
  },
  {
    img: artwork08Solution,
    pageNo: 8,
    title: 'Alien Cult',
  },
  {
    img: artwork09Solution,
    pageNo: 9,
    title: 'Psychedelic Dream',
  },
  {
    img: artwork10Solution,
    pageNo: 10,
    title: 'Las Vegas',
  },
  {
    img: artwork11Solution,
    pageNo: 11,
    title: 'Stoned Ape Theory',
  },
  {
    img: artwork12Solution,
    pageNo: 12,
    title: 'Trippy Stairs',
  },
  {
    img: artwork13Solution,
    pageNo: 13,
    title: 'Desert of Mescaline',
  },
  {
    img: artwork14Solution,
    pageNo: 14,
    title: 'Aliens At War',
  },
  {
    img: artwork15Solution,
    pageNo: 15,
    title: 'Interspecies Party',
  },
  {
    img: artwork16Solution,
    pageNo: 16,
    title: 'Yoga Retreat',
  },
  {
    img: artwork17Solution,
    pageNo: 17,
    title: 'The Feeling of Being Watched',
  },
  {
    img: artwork18Solution,
    pageNo: 18,
    title: 'Utopian City',
  },
  {
    img: artwork19Solution,
    pageNo: 19,
    title: 'Psilocybin Creek',
  },
  {
    img: artwork20Solution,
    pageNo: 20,
    title: 'Psychedelic Festival',
  },
  {
    img: artwork21Solution,
    pageNo: 21,
    title: 'Woodstock Inspired Festival',
  },
  {
    img: artwork22Solution,
    pageNo: 22,
    title: 'Ayahuasca Hallucinations',
  },
];
