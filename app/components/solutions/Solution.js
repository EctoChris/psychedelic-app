import { forwardRef, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { RingLoader } from 'react-spinners';

const Solution = forwardRef(({ img, imgSrc, title, pageNo, index, onHover, onClick }, ref) => {
  const [loading, setLoading] = useState(true);
  const renderCount = useRef(0);
  const imgLoading = loading && renderCount.current < 2;

  return (
    <div
      className='flex-col p-5 rounded-sm bg-psyBlue centered hover:cursor-pointer border-2 border-white hover:border-psyOrange duration-150 transition'
      onMouseEnter={() => onHover(index)}
      ref={ref}
      onClick={() => onClick(index)}
    >
      <span className='invisible'>{renderCount.current++}</span>

      {imgLoading && (
        <div className='centered w-full h-full'>
          <RingLoader size={25} />
        </div>
      )}
      <img src={img} alt={title} onLoad={() => setLoading(false)} />
      <span className='py-2'>
        Page: {pageNo} - {title}
      </span>
    </div>
  );
});

Solution.propTypes = {
  img: PropTypes.string.isRequired,
  imgSrc: PropTypes.string,
  index: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  pageNo: PropTypes.number.isRequired,
};

export default Solution;
