import PropTypes from 'prop-types';
import Overlay from './Overlay';

const InnerContainer = ({ backgroundMobile, backgroundTablet, backgroundDesktop, children }) => {
  return (
    <>
      <div
        className='hidden h-180 bg-center bg-cover md:block lg:hidden'
        style={{ backgroundImage: `url(${backgroundTablet})` }}
      >
        {children}
      </div>
      <div
        className='bg-center bg-cover xs:block sm:block md:hidden h-180'
        style={{ backgroundImage: `url(${backgroundMobile})` }}
      >
        {children}
      </div>
      <div
        className='hidden bg-center bg-cover lg:block h-180 '
        style={{ backgroundImage: `url(${backgroundDesktop})` }}
      >
        {children}
      </div>
    </>
  );
};

const Header = ({ backgroundDesktop, backgroundTablet, backgroundMobile, children }) => {
  return (
    <InnerContainer
      backgroundDesktop={backgroundDesktop}
      backgroundMobile={backgroundMobile}
      backgroundTablet={backgroundTablet}
    >
      <Overlay>{children}</Overlay>
    </InnerContainer>
  );
};

Header.propTypes = {
  backgroundMobile: PropTypes.string,
  backgroundDesktop: PropTypes.string,
  backgroundTablet: PropTypes.string,
  children: PropTypes.any,
};

export default Header;
