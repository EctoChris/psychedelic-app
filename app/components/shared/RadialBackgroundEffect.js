/** @jsx jsx */
import { jsx, css } from '@emotion/react';
import React from 'react';
import PropTypes from 'prop-types';
import { useMousePos } from '../../hooks/useMousePos.js';

const RadialBackgroundEffect = ({ children }) => {
  const { x, y, winInnerWidth, winInnerHeight } = useMousePos();
  const xPerc = (x / winInnerWidth) * 100;
  const yPerc = (y / winInnerHeight) * 100;
  const classes = useStyles({ xPerc, yPerc });

  return <div css={classes.container}>{children}</div>;
};

const useStyles = ({ xPerc, yPerc }) => {
  return {
    container: css`
    background-image: linear-gradient(#71FFDE, #06C3F6);
    background: radial-gradient(at ${xPerc}% ${yPerc}%, #71FFDE, #06C3F6); 
    bg-no-repeat;
    z-index: 10;
    `,
  };
};

RadialBackgroundEffect.propTypes = {
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

export default RadialBackgroundEffect;
