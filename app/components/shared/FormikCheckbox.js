import React, { useState } from 'react';
import PropTypes from 'prop-types';

const FormikCheckbox = ({ name, placeholder, formik, submissionAttempt }) => {
  const [focusAway, setFocusAway] = useState(false);
  const error = formik.errors[name];
  const showErrorMessage = (error && focusAway) || (error && submissionAttempt);

  return (
    <div className='relative w-72 mb-4'>
      <input
        type='checkbox'
        id={name}
        onFocus={() => formik.setTouched(name, true)}
        placeholder={placeholder}
        name={name}
        onChange={formik.handleChange}
        onBlur={() => setFocusAway(true)}
        className={`w-full h-5 px-4 overflow-hidden text-sm bg-gray-100 hover:cursor-pointer`}
      />
      <label htmlFor={name} className='hover:cursor-pointer text-xxs mb-2'>
        {placeholder}
      </label>
      <div className='centered'>
        <p className={`text-red-600 text-xs absolute`}>{showErrorMessage && formik.errors[name]}</p>
      </div>
    </div>
  );
};

FormikCheckbox.propTypes = {};

export default FormikCheckbox;
