import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { useScreenDimensions } from '../../hooks/useScreenDimensions.js';
import Confetti from '../../models/Confetti.model.js';

const ConfettiEffect = ({ children }) => {
  const containerRef = useRef(null);
  const canvasRef = useRef(null);
  const animationRef = useRef(null);
  const { width, height } = useScreenDimensions();
  const NUM_OF_CONFETTI = 50;

  useEffect(() => {
    // Get dimensions on window resize and setup canvas:
    const canvas = canvasRef.current;
    const ctx = canvas.getContext('2d');
    canvas.width = containerRef.current.offsetWidth || 0;
    canvas.height = containerRef.current.offsetHeight || 0;

    // Generate confetti circles:
    const circles = [];
    for (let i = 0; i < NUM_OF_CONFETTI; i++) {
      circles.push(new Confetti(width, height, ctx));
    }

    const renderConfetti = () => {
      // Clear canvas from previous frame:
      ctx.clearRect(0, 0, canvas.width, canvas.height);

      // Draw all confetti:
      circles.forEach((circle) => {
        circle.draw();
      });

      // Save reference to current animation frame to cleanup later:
      animationRef.current = requestAnimationFrame(renderConfetti);
    };
    if (ctx) renderConfetti();

    // Cleanup function to cancel animation on component close:
    return function cleanup() {
      cancelAnimationFrame(animationRef.current);
    };
  }, [width, height]);

  return (
    <div className='relative' ref={containerRef}>
      <canvas ref={canvasRef} className='absolute pointer-events-none z-80' />
      {children}
    </div>
  );
};

ConfettiEffect.propTypes = {
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

export default ConfettiEffect;
