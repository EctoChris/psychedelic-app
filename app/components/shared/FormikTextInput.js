import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';

const inputStates = {
  error: 'border-red-400 border-4',
  success: 'border-green-400 border-4',
};

const FormikTextInput = ({ name, placeholder, formik, submissionAttempt }) => {
  const [touched, setTouched] = useState(false);
  const [focusAway, setFocusAway] = useState(false);
  const error = formik.errors[name];
  const valid = !error && formik.values[name].length > 0;

  useEffect(() => {
    if (Boolean(formik.touched === name)) {
      setTimeout(() => setTouched(true), 300);
    }
  }, [formik.touched]);

  let borderClass = '';
  if ((error && touched) || (error && submissionAttempt)) borderClass = inputStates.error;
  else if (valid) borderClass = inputStates.success;

  const showErrorMessage = (error && touched && focusAway) || (error && submissionAttempt);

  const WIDTH = 72;
  return (
    <div className='relative flex-col my-5'>
      <div className={`relative w-${WIDTH} overflow-hidden group`}>
        <input
          type='text'
          onFocus={() => formik.setTouched(name, true)}
          placeholder={placeholder}
          name={name}
          onChange={formik.handleChange}
          onBlur={() => setFocusAway(true)}
          className={`w-full h-10 px-4 overflow-hidden text-sm bg-gray-100  outline-none peer ${borderClass}`}
        />
        <span
          className={`absolute bottom-0 left-0 w-0 h-1 transition-all duration-300 border-0 border-b-4 border-b-psyBlue peer-focus:w-full`}
        ></span>
        <span
          className={`absolute top-0 left-0 w-4 h-0 transition-all duration-300 border-0 border-l-4 border-l-psyBlue peer-focus:h-full`}
        ></span>
        <span
          className={`absolute right-0 w-4 h-10 transition-all duration-300 border-0 border-r-4 top-10 border-r-psyBlue peer-focus:top-0`}
        ></span>
        <span
          className={`absolute top-0 w-full h-0.5 transition-all duration-300 border-0 border-t-4 left-${WIDTH} border-t-psyBlue peer-focus:left-0`}
        ></span>
      </div>
      <p className={`text-red-600 text-xs absolute`}>{showErrorMessage && formik.errors[name]}</p>
    </div>
  );
};

FormikTextInput.propTypes = {
  name: PropTypes.string.isRequired,
  formik: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  placeholder: PropTypes.string,
  submissionAttempt: PropTypes.bool,
};

export default FormikTextInput;
