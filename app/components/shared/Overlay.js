const Overlay = ({ children, opacityStr, radial }) => {
  return radial ? (
    <div
      className='flex items-center justify-center w-full h-full bg-opacity-40'
      style={{ background: 'radial-gradient(rgba(255,255,255,0), #000);' }}
    >
      {children}
    </div>
  ) : (
    <div className={`flex-1 w-full h-full bg-black bg-opacity-5 flex justify-center items-center`}>{children}</div>
  );
};
export default Overlay;
