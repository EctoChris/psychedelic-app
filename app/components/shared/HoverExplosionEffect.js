import { useEffect, useRef } from 'react';
import { useScreenDimensions } from '../../hooks/useScreenDimensions.js';
import Explosion from '~/models/Explosion.model.js';

const range = (a, b) => {
  return ~~((b - a) * Math.random() + a);
};

const HoverTailEffect = ({ children }) => {
  const containerRef = useRef(null);
  const canvasRef = useRef(null);
  const animationRef = useRef(null);
  const { width, height } = useScreenDimensions();
  const NUM_OF_EXPLOSIONS = 10;

  useEffect(() => {
    // Get dimensions on window resize and setup canvas:
    const canvas = canvasRef.current;
    const ctx = canvas.getContext('2d');
    canvas.width = containerRef.current.offsetWidth || 0;
    canvas.height = containerRef.current.offsetHeight || 0;

    // Generate confetti circles:
    const circles = [];
    for (let i = 0; i < NUM_OF_EXPLOSIONS; i++) {
      circles.push(new Explosion(width, height, ctx, ~~range(10, 300), ~~range(10, 300)));
    }

    const renderConfetti = () => {
      // Clear canvas from previous frame:
      ctx.clearRect(0, 0, canvas.width, canvas.height);

      // Draw all confetti:
      circles.forEach((circle) => {
        circle.draw();
      });

      // Save reference to current animation frame to cleanup later:
      animationRef.current = requestAnimationFrame(renderConfetti);
    };
    if (ctx) renderConfetti();

    // Cleanup function to cancel animation on component close:
    return function cleanup() {
      cancelAnimationFrame(animationRef.current);
    };
  }, [width, height, canvasRef]);
  return (
    <div ref={containerRef}>
      <canvas ref={canvasRef}>{children}</canvas>;
    </div>
  );
};

export default HoverTailEffect;
