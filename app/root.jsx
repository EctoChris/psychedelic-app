import { Links, LiveReload, Meta, Outlet, Scripts } from 'remix';
import tailwindStyles from '~/styles/main.css';
import Navbar from './components/layout/Navbar';
import Footer from './components/layout/Footer';
// import ModalDisclaimer from './components/layout/ModalDisclaimer';

{
  /* <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Miltonian+Tattoo&display=swap" rel="stylesheet"></link> */
}
{
  /* <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Fugaz+One&display=swap" rel="stylesheet"></link> */
}
// Loading in stylesheets:
export const links = () => {
  return [
    { rel: 'stylesheet', href: tailwindStyles },
    { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
    { rel: 'preconnect', href: 'https://fonts.gstatic.com' },
    { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Ubuntu+Mono&display=swap' },
  ];
};

export const meta = () => {
  const description = 'A psychedelic hidden picture book solutions website for Susan Byron.';
  const keywords = 'stoner, activity book, hidden picture, psychedelic art';

  return {
    description,
    keywords,
  };
};

export default function App() {
  return (
    <html lang='en'>
      <head>
        <script
          dangerouslySetInnerHTML={{
            __html: `
                        !function(t,n,e,o,a){function d(t){var n=~~(Date.now()/3e5),o=document.createElement(e);o.async=!0,o.src=t+"?ts="+n;var a=document.getElementsByTagName(e)[0];a.parentNode.insertBefore(o,a)}t.MooTrackerObject=a,t[a]=t[a]||function(){return t[a].q?void t[a].q.push(arguments):void(t[a].q=[arguments])},window.attachEvent?window.attachEvent("onload",d.bind(this,o)):window.addEventListener("load",d.bind(this,o),!1)}(window,document,"script","//cdn.stat-track.com/statics/moosend-tracking.min.js","mootrack");
                        //tracker has to be initialized otherwise it will generate warnings and wont sendtracking events
                        mootrack('init', '825979f7-a5ec-4646-999a-27ad88104d21');
                        mootrack('trackPageView');
                      `,
          }}
        />
        <Links />
        <title>Where's My Dealer</title>
        <meta charSet='utf-8' />
        <meta name='viewport' content='width=device-width,initial-scale=1' />

        <Meta />
      </head>
      <body className='debug-screens'>
        <Navbar />
        <Outlet />
        <Scripts />
        <Footer />
        {/* <ModalDisclaimer /> */}
        {process.env.NODE_ENV === 'development' && <LiveReload />}
      </body>
    </html>
  );
}
