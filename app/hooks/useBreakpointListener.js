import { useEffect, useState } from 'react';
import resolveConfig from 'tailwindcss/resolveConfig';
import tailwindConfig from '../../tailwind.config'

const fullConfig = resolveConfig(tailwindConfig);

export const useBreakpointListener = () => {
  const [currentBreakpoint, setCurrentBreakpoint] = useState(null);

  const convertBreakpointToNumber = (breakpoint) => parseInt(fullConfig.theme.screens[breakpoint].substr(0, fullConfig.theme.screens[breakpoint].length-2))

  const assignBreakpoint = () => {
    if(window.innerWidth >= convertBreakpointToNumber('2xl')) setCurrentBreakpoint('2xl')
    else if(window.innerWidth <= convertBreakpointToNumber('xs')) setCurrentBreakpoint('xs')
    else if(window.innerWidth <= convertBreakpointToNumber('sm')) setCurrentBreakpoint('xs')
    else if(window.innerWidth <= convertBreakpointToNumber('md')) setCurrentBreakpoint('sm')
    else if(window.innerWidth <= convertBreakpointToNumber('lg')) setCurrentBreakpoint('md')
    else if(window.innerWidth <= convertBreakpointToNumber('xl')) setCurrentBreakpoint('lg')
    else if(window.innerWidth < convertBreakpointToNumber('2xl')) setCurrentBreakpoint('xl')
  }

  useEffect(() => {
    if(currentBreakpoint === null) assignBreakpoint()
    const resizeHandler = () =>  assignBreakpoint()
    
    window.addEventListener('resize', resizeHandler);
    return () => window.removeEventListener('resize', resizeHandler);
  }, []);

  return currentBreakpoint;
};
