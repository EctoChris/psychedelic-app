import { useEffect, useState } from 'react';

export const useCursorPos = () => {
  const [cursorPos, setCursorPos] = useState({ xPosOffsetPercentage: 0, yPosOffsetPercentage: 0 });

  const getMouseCoords = (e) => {
    const { innerWidth: windowWidth, innerHeight: windowHeight } = e.view;
    const midScreenX = windowWidth / 2;
    const midScreenY = windowHeight / 2;
    let xOffset = Math.abs(midScreenX - e.clientX);
    let yOffset = Math.abs(midScreenY - e.clientY);

    let xPosOffsetPercentage = xOffset / midScreenX;
    let yPosOffsetPercentage = yOffset / midScreenY;

    if (e.clientX >= midScreenX) xPosOffsetPercentage = -xPosOffsetPercentage;
    if (e.clientY >= midScreenY) yPosOffsetPercentage = -yPosOffsetPercentage;

    return { xPosOffsetPercentage, yPosOffsetPercentage };
  };

  useEffect(() => {
    const handleMouseMove = (e) => {
      const MIN_DISTANCE_TO_MOVE = 100;

      if (
        Math.abs(e.clientX - cursorPos.lastX) >= MIN_DISTANCE_TO_MOVE ||
        Math.abs(e.clientY - cursorPos.lastY) >= MIN_DISTANCE_TO_MOVE
      )
        setCursorPos({ ...getMouseCoords(e), lastX: e.clientX, lastY: e.clientY });
    };

    window.addEventListener('mousemove', handleMouseMove);
    return () => window.removeEventListener('mousemove', handleMouseMove);
  }, []);

  return cursorPos;
};
