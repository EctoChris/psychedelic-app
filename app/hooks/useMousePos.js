import { useEffect, useState } from 'react';

export const useMousePos = () => {
  const [cursorPos, setCursorPos] = useState({ x: 0, y: 0 });

  useEffect(() => {
    const handleMouseMove = (e) =>
      setCursorPos({
        x: e.clientX,
        y: e.clientY,
        winInnerWidth: window.innerWidth,
        winInnerHeight: window.innerHeight,
      });
    window.addEventListener('mousemove', handleMouseMove);
    return () => window.removeEventListener('mousemove', handleMouseMove);
  }, []);

  return cursorPos;
};
