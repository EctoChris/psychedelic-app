import PropTypes from 'prop-types';
import { useLocation } from 'react-router-dom';

// Hook is called with the syntax:
// const { coupon_code } = useQuery(['coupon_code'])

const useQuery = (paramsArray) => {
  const query = new URLSearchParams(useLocation().search);
  const queryObj = {};

  paramsArray.forEach((param) => {
    queryObj[param] = query.get(param);
  });
  return queryObj;
};

useQuery.propTypes = {
  paramsArray: PropTypes.array,
};

export default useQuery;
