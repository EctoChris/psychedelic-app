import { useEffect, useState } from 'react';

export const useScreenDimensions = () => {
  const [screenDimensions, setScreenDimensions] = useState({ width: 0, height: 0 });

  useEffect(() => {
    const resizeHandler = () => setScreenDimensions({ width: window.innerWidth, height: window.innerHeights });
    window.addEventListener('resize', resizeHandler);
    return () => window.removeEventListener('resize', resizeHandler);
  }, []);

  return screenDimensions;
};
