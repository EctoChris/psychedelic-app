import { useEffect, useState } from 'react';
import resolveConfig from 'tailwindcss/resolveConfig';
import tailwindConfig from '../../tailwind.config';

const fullConfig = resolveConfig(tailwindConfig);

function getBreakpointValues() {
  return Object.entries(fullConfig.theme.screens).map((breakpoint) => {
    return { [breakpoint[0]]: parseInt(breakpoint[1].replace(/[^0-9\.]+/g, '')) };
  });
}

const UseBreakpoints = () => {
  const [windowSize, setWindowSize] = useState({ width: 0, height: 0 });

  const handleSize = () => {
    setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight,
    });
  };

  useEffect(() => {
    handleSize();

    window.addEventListener('resize', handleSize);

    return () => window.removeEventListener('resize', handleSize);
  }, []);

  return windowSize;
};

export default UseBreakpoints;
