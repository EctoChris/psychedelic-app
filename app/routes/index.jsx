import Header from '~/components/shared/Header';
import psyBackLg from '../assets/home/PsyHeaderTablet.jpg';
import psyBackSm from '../assets/home/PsyHeaderMobile.jpg';
import psyBackTablet from '../assets/home/PsyHeaderDesktop.jpg';
import TitleBanner from '~/components/home/TitleBanner';
import BrownieEmailBanner from '~/components/home/BrownieEmailBanner';
import SolutionsBanner from '~/components/home/SolutionsBanner';
import RadialBackgroundEffect from '../components/shared/RadialBackgroundEffect';
import { json } from 'remix';
import axios from 'axios';

const EMAIL_LIST_ID = 'e9c5332d-c5fa-4a81-8651-11e0514c8a88';

async function googleVerification(captchaToken) {
  try {
    const validation = await axios.post(
      `https://www.google.com/recaptcha/api/siteverify?secret=${process.env.REACT_APP_GOOGLE_RECAPTCHA_SECRET_KEY}&response=${captchaToken}`
    );

    if (!validation.data.success) {
      throw new Error('Invalid captcha');
    }
    return validation;
  } catch (error) {
    throw new Error('Invalid captcha.');
  }
}

export async function action({ request }) {
  // Get form data from userinput on FE:
  const body = await request.formData();
  const name = body.get('name');
  const email = body.get('email');
  const token = body.get('token');
  const HasExternalDoubleOptIn = false;
  const CustomFields = ["book_interest=Where's my Dealer v1"];

  await googleVerification(token);

  // Example MOOSEND API RESPONSE:
  // http://api.moosend.com/v3/campaigns/create.json?apikey=YOUR_API_KEY
  // https://api.moosend.com/v3/subscribers/a589366a-1a34-4965-ac50-f1299fe5979e/subscribe.json?apikey=bafe55f4-e53e-4651-8164-c6d6ff05081b
  const rawResponse = await fetch(
    `https://api.moosend.com/v3/subscribers/${EMAIL_LIST_ID}/subscribe.json?apikey=${process.env.REACT_APP_MOOSEND_API_KEY}`,
    {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ name, email, HasExternalDoubleOptIn, CustomFields }),
    }
  );
  const res = await rawResponse.json();
  console.log('res.status: ', res);
  return json({
    message: `Success! You should receive an email within the next 30 minutes! Please check your spam folder`,
  });
}

function Home() {
  <ErrorBoundary />;
  return (
    <div>
      <Header backgroundDesktop={psyBackLg} backgroundTablet={psyBackTablet} backgroundMobile={psyBackSm}>
        <TitleBanner />
      </Header>
      <RadialBackgroundEffect>
        <SolutionsBanner />
      </RadialBackgroundEffect>
      <BrownieEmailBanner />
    </div>
  );
}

export function ErrorBoundary({ error }) {
  console.log(error);
  return (
    <RadialBackgroundEffect>
      <div className='centered flex flex-col mx-auto p-4 overflow-hidden h-screen text-center'>
        <h1 className='my-8'>Error</h1>
        <p>{error.message}</p>
        <p>
          Please be a legend and email{' '}
          <a href='mailto: chris@psypublishing.com' className='text-blue-700'>
            chris@psypublishing.com
          </a>{' '}
          for assistance.
        </p>
      </div>
    </RadialBackgroundEffect>
  );
}

export default Home;
