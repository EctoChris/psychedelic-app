import { Outlet } from 'remix';
import TermsHeader from '../components/terms/TermsHeader';
import { WEBSITE_URL } from '../constants/constants';
import { useEffect } from 'react';

function Terms() {
  useEffect(() => {
    window.scrollTo({ top: 0 });
  }, []);
  return (
    <div>
      <div className='w-full text-center bg-psyBlue text-black text-boldest p-2'>
        <h1 className='py-4'>Terms and Conditions</h1>
        <span>PLEASE READ THESE TERMS OF USE CAREFULLY BEFORE YOU USE THIS WEBSITE</span>
        <p>USE OF '{WEBSITE_URL}' WEBSITE</p>
      </div>
      <div className='max-w-screen-lg mx-auto text-left font-sans pt-2'>
        <TermsHeader title='1. Terms of Website Use' />
        <p>
          {WEBSITE_URL} is a website operated by Christopher Ford (the “Sole Trader”). Christopher Ford is a Sole Trader
          registered in Australia under Australian Business Number (ABN) 95 602 808 910.
        </p>
        <p className='text-red-400'>
          NOTE:- YOU SHOULD NOT USE THIS SITE HOSTED AT DOMAIN WWW.PSYCH-APP.VERCEL.APP IF YOUR GOVERNING LAWS DO NOT
          ALLOW MEDICAL OR RECREATIONAL USE OF CANNABIS. 
        </p>
        <p>
          These terms of use (“agreement”) sets forth the legally binding terms for your use of the site and services
          provided at www.Psych-app.Vercel.App. By accessing or using the site or downloading the free e-book or any
          other services, you are agreeing to this agreement and you recognize and permit that you have the right,
          authority, and capacity to enter into this agreement. If you do not have the capacity to enter into this
          agreement, you may not access or use the site or services or accept the agreement. If you do not agree with
          all of the key provisions of this agreement, do not access and/or use the site or services.
        </p>

        <p>
          If you are using the site or services on behalf of a company, entity, or organization, you acknowledge and
          agree that you are an authorized representative of such company, entity, or organization with the authority to
          bind it to this agreement. Please read these terms carefully, as they contain an agreement to arbitrate as
          well as other important information about your legal rights, remedies, and obligations. The arbitration
          agreement requires (with limited exception) that you submit any claims you may have against us to binding and
          final arbitration, and further (1) you will only be eligible to pursue claims against the company on an
          individual basis, not as a plaintiff or class member in any class or representative action or proceeding, and
          (2) you will be entitled to relief (including monetary, injunctive, and declaratory relief) on an individual
          basis only.
        </p>

        <TermsHeader title='Important Disclaimers' />
        <p>
          All information contained on the site and services is for informational, recreational, and/or educational
          Purposes only. It is not intended to be a substantive for medical advice, diagnosis, or treatment. Always
          Consult a qualified doctor for any medical questions. By accessing and using the site you have the
          Responsibility to abide by governing laws for the use of cannabis in your jurisdiction. The Sole Trader does
          not endorse and is not responsible for the accuracy or reliability of, any opinion, advice, statement, or
          other Information made on the site or services, including user content and third-party materials.
        </p>

        <TermsHeader title='2. Changes to these Terms and additional terms' />
        <p>
          <b>2.1. </b>The Sole Trader may change these Terms from time to time. The Sole Trader will post the updated
          Terms on the Website, and they will take effect immediately.
        </p>
        <p>
          <b>2.2. </b>
          Your ongoing use of the Website after these Terms have changed will be treated as your acceptance of the
          updated Terms, so you should check these Terms regularly.
        </p>
        <TermsHeader title={`4. The Sole Trader’s liability`} />
        <p>
          <b>4.1. </b>
          The Website is provided on an “as is” basis so whilst the Sole Trader endeavours to ensure the accuracy of the
          information placed on the Website the Sole Trader does not warrant or guarantee the accuracy or correctness of
          such information or of any other content, description or material place or referred to in the Website. The
          Sole Trader does not warrant that the website, its content or the server that makes it available are error or
          virus free of other harmful components or that your use of the Website will be uninterrupted.
        </p>
        <p>
          <b>4.2. </b>
          The Sole Trader will not be liable to you (whether under the law of contract, the law of torts or otherwise)
          in relation to the contents of, or use of, or otherwise in connection with, this website:
        </p>
        <p>
          <b>4.2.1. </b>
          for any direct loss
        </p>
        <p>
          <b>4.2.2. </b>
          for any indirect, special or consequential loss; or
        </p>
        <p>
          <b>4.2.3. </b>
          for any business losses, loss of revenue, income, profits or anticipated savings, loss of contracts or
          business relationships, loss of reputation or goodwill, or loss or corruption of information or data.
        </p>
        <p>
          <b>4.2.4. </b>
          any loss or damage which may be incurred by you as a result of:
        </p>
        <p>
          <b>4.2.4.1. </b>
          any changes that the Sole Trader makes to the Website, or for any permanent or temporary cessation in the
          provision of the Website or any part of it;
        </p>
        <p>
          <b>4.2.4.2. </b>
          the deletion of, corruption of, or failure to store any Submissions (as defined below) (and other data)
          maintained or transmitted by or through your use of the Website;
        </p>
        <p>
          <b>4.3. </b>
          The limitations of liability in Clause 4.2. above apply even if the Sole Trader has been expressly advised of
          the potential loss.
        </p>
        <p>
          <b>4.4. </b>
          Any expressions of opinion or statements of fact made by persons or organisations on the Website are made by
          those entities in a personal capacity and shall not be construed as being authorised by, or approved by the
          Sole Trader. The Website is an innocent disseminator of information.
        </p>
        <p>
          <b>4.5. </b>
          The Sole Trader does not limit his liability for death or personal injury resulting from the Sole Trader’s
          negligence, fraudulent misrepresentation or any other losses which may not be lawfully excluded or limited by
          law.
        </p>
        <p>
          <b></b>
        </p>
        <TermsHeader title='3. Your Liability' />
        <p>
          You agree to refund or otherwise take responsibility for any and all liabilities, costs and expenses,
          including reasonable legal fees, which the Sole Trader or any of the Sole Trader’s personnel may incur as a
          result of any breach of these Terms by you, or in connection with the use of the Website or any submission or
          other transmission of any message or information on the Website by you.
        </p>
        <TermsHeader title='4. Eligibility' />
        <p>
          You must be 21 years or above 21 of age and resident in a jurisdiction that allows the recreational and
          medical use of marijuana to access the content of the site.
        </p>
        <TermsHeader title='5. Linking From the Site' />
        <p>
          <b>5.1. </b>
          The Website may provide hyperlinks to other websites which are not owned by the Sole Trader, the content of
          which is not in his control. Accordingly the Sole Trader cannot accept any responsibility for the content,
          materials (including goods and services) or practices featured on such websites. Links to such websites are
          not to be taken as the Sole Trader’s endorsement of them or any content within them or as warranty that such
          websites will be free of viruses or other such items of a destructive nature.
        </p>
        <p>
          <b>5.2. </b>
          The Sole Trader would advise you to read the terms and conditions and privacy notice of any website you visit
          when you leave this Website.
        </p>
        <TermsHeader title='6. Use of the Website Content' />
        <p>
          <b>6.1. </b>
          All content which features on the Website which is provided by the Sole Trader or on his behalf including but
          not limited to ebooks, worksheets, puzzles, text, photographs, images, graphics, illustrations, sounds,
          videos, designs, written and other material (together, “Content”) is protected by copyright, trade mark rights
          and/or other intellectual property rights owned or licensed to the Sole Trader.
        </p>
        <p>
          <b>6.2. </b>
          You are permitted to download, print or copy any Content from the Website, provided that you:
        </p>
        <p>
          <b>6.2.1. </b>
          do so only for your personal, non-commercial use;
        </p>
        <p>
          <b>6.2.2. </b>acknowledge the Sole Trader as the source of the material; and
        </p>
        <p>
          <b>6.2.3. </b>
          do not use the Content in any other manner.
        </p>
        <p>
          <b>6.3. </b>
          We reserve any other uses of, and rights in, the Content so you must ask the Sole Trader’s prior permission
          for these as set out above. The Sole Trader will not always be able to give his consent.
        </p>
        <p>
          <b>6.4. </b>You must not, nor try to, make mass, automated or systematic extractions of the Content, or use it
          to create or include it within another paper or electronic database, or try to re-sell or re-distribute it.
          The Sole Trader reserves the right to prohibit or restrict the way in which other sites link to or frame or
          re-present any of the Content.
        </p>
        <TermsHeader title='7. Terms held to be invalid or unenforceable' />
        <p>
          If any provision of these Terms is held to be invalid or unenforceable by a court of law with jurisdiction to
          decide on this matter, then such provision shall be removed from these Terms without affecting the rest of
          these Terms, and the remaining provisions shall continue to be valid and enforceable.
        </p>
        <TermsHeader title={`8. Waiver of the Sole Trader's Rights`} />
        <p>
          A failure by the Sole Trader in exercising his rights or remedies which arise under these Terms shall not be a
          waiver of that right or remedy, and no waiver by the Sole Trader shall be effective unless provided in writing
          and signed by the Sole Trader.
        </p>
        <TermsHeader title='9. Materials submitted to the Sole Trader' />
        <p>
          By submitting any materials in connection with the Website or any service offered by the Sole Trader, you
          agree that any and all comments, messages, postings, data, suggestions, creative ideas, designs, concepts,
          product suggestions or other materials (including the intellectual property in such works and materials)
          submitted or offered to the Sole Trader (“Submissions”) shall be treated as non-confidential and not
          proprietary to you and shall become, and remain, Sole Trader property to be used for his marketing,
          promotional, branding and commercial purposes, and without payment, royalty or other consideration. Your
          participation in, or disclosure of, any Submission provides the Sole Trader with the permission to edit,
          alter, copy, exhibit, publish or distribute the Submission for any lawful purpose. You warrant that the
          Submission is original and has not been copied wholly or substantially from any other designs or works and
          that the use of reproduction of the Submission will not infringe the copyright or any other intellectual
          property rights of any third party. You waive the right to inspect or approve the finished product where the
          Submission appears. By your submission, you confirm that you (i) have read and understand the above terms and
          (ii) are at least 21 years of age, (iii) reside in a jurisdiction that allows the recreational and medical use
          of marijuana.
        </p>
        <br />
        <br />

        <Outlet />
      </div>
    </div>
  );
}

export default Terms;
