import Solution from '../components/solutions/Solution';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
/** @jsx jsx */
import { jsx, css } from '@emotion/react';

import ImageViewer from 'react-simple-image-viewer';
import RadialBackgroundEffect from '../components/shared/RadialBackgroundEffect';
import { useBreakpointListener } from '../hooks/useBreakpointListener';
import { solutionsArray } from '../components/solutions/SolutionsData.js';

const solutions = () => {
  const [lastRowAndCol, setLastRowAndCol] = useState({ row: null, col: null });
  const gridItemRef = useRef(null);
  const [currentImage, setCurrentImage] = useState(0);
  const [isViewerOpen, setIsViewerOpen] = useState(false);
  const currentBreakpoint = useBreakpointListener();

  useEffect(() => {
    window.scrollTo({ top: 0 });
  }, []);

  const images = useMemo(() => solutionsArray.map((solution) => solution.img));

  const openImageViewer = useCallback((index) => {
    setCurrentImage(index);
    setIsViewerOpen(true);
  }, []);

  const closeImageViewer = () => {
    setCurrentImage(0);
    setIsViewerOpen(false);
  };

  let NUM_OF_ROWS = 6;
  let NUM_OF_COLS = 4;
  if (currentBreakpoint === 'md') {
    NUM_OF_ROWS = 8;
    NUM_OF_COLS = 3;
  } else if (currentBreakpoint === 'xs' || currentBreakpoint === 'sm') {
    NUM_OF_ROWS = 22;
    NUM_OF_COLS = 1;
  }

  const setRowAndCol = (index) => {
    const row = ~~(index / NUM_OF_COLS);
    const col = index % NUM_OF_COLS;
    setLastRowAndCol({ row, col });
  };

  const classes = useStyles({
    row: lastRowAndCol.row,
    col: lastRowAndCol.col,
    numOfRows: NUM_OF_ROWS,
    numOfCols: NUM_OF_COLS,
    gridItemWidth: gridItemRef?.current?.offsetWidth,
    gridItemHeight: gridItemRef?.current?.offsetHeight,
  });

  return (
    <RadialBackgroundEffect>
      <div className='min-w-full min-h-full'>
        <div className='text-center py-3'>
          <h2 className='font-bold'>Solutions</h2>
        </div>
        <div className='relative grid w-10/12 grid-cols-1 grid-rows-22 xs:grid-rows-22 xs:grid-cols-1 sm:grid-rows-22 sm:grid-cols-1 md:grid-rows-8 md:grid-cols-3 lg:grid-rows-6 lg:grid-cols-4 mx-auto my-auto'>
          {solutionsArray.map((solution, i) => {
            return (
              <Solution
                key={i}
                img={solution.img}
                imgSrc={solution?.imgSrc}
                title={solution.title}
                pageNo={solution.pageNo}
                index={i}
                onHover={(v) => setRowAndCol(v)}
                ref={gridItemRef}
                onClick={openImageViewer}
              />
            );
          })}
          <div css={classes.movingBorder}></div>
        </div>
        {isViewerOpen && (
          <ImageViewer
            src={images}
            currentIndex={currentImage}
            onClose={closeImageViewer}
            disableScroll={false}
            backgroundStyle={{
              backgroundColor: 'rgba(0,0,0,0.9)',
            }}
            closeOnClickOutside={true}
          />
        )}
      </div>
      <br />
    </RadialBackgroundEffect>
  );
};

const useStyles = ({ row, col, numOfRows, numOfCols, gridItemWidth, gridItemHeight }) => {
  const top = row !== null ? row * (100 / numOfRows) : 45;
  const left = col !== null ? col * (100 / numOfCols) : 45;
  return {
    movingBorder: css`
      position: absolute;
      width: ${gridItemWidth}px;
      height: ${gridItemHeight}px;
      border: 4px solid black;
      border-radius: 4px;
      -webkit-transition: left 1s ease-out, top 1s ease-out;
      transition: left 0.5s ease-out, top 0.5s ease-out;
      pointer-events: none;
      top: ${top}%;
      left: ${left}%;
    `,
  };
};

export default solutions;
