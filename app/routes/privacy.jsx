import TermsHeader from '../components/terms/TermsHeader';
import { useEffect } from 'react';

function Privacy() {
  useEffect(() => {
    window.scrollTo({ top: 0 });
  }, []);
  return (
    <div>
      <div className='w-full text-center bg-psyBlue text-black text-boldest p-2'>
        <h1 className='py-4'>Privacy Notice for Users of the Website</h1>
      </div>
      <div className='max-w-screen-lg mx-auto text-left font-sans pt-2'>
        <h2 className='text-psyOrange'>Introduction</h2>
        <p>
          The UK General Data Protection Regulation (<b>“UK GDPR”</b>) and the Privacy and Electronic Communications (
          <b>“PECR”</b>) seek to protect and enhance the rights of data subjects. These rights cover the safeguarding of
          personal data and protection against the unlawful processing of personal data.
        </p>
        <p>
          Christopher Ford, a Sole Trader registered in Australia under Australian business number (ABN) 95 602 808 910
          is the personal data controller (hereinafter <b>“Controller”</b>) for the processing of your personal data.
        </p>
        <p>The Controller takes your privacy seriously and is committed to protecting your personal information.</p>
        <p>
          This Privacy Notice explains what personal information the Controller collects, how does he use that personal
          information, with whom does he share it, and in what circumstances.
        </p>
        <p>
          The Controller is pleased to provide this Privacy Notice which covers this website (<b>“Website”</b>). You
          might find external links to third party websites on the Website and those websites may have their own privacy
          notices which are different to this Privacy Notice. This Privacy Notice does not apply to your use of a third
          party site and you should therefore make sure that you are comfortable with their privacy notices prior to
          using those sites.
        </p>
        <TermsHeader title='Categories of personal data. Legal grounds' />
        <p>
          The categories of personal data of users of the Website and the legal grounds on which this personal data
          processing is based upon are as follows:
        </p>
        <h4 className='text-psyOrange'>
          <b>Information from the email subscription form</b>
        </h4>
        <table className='table-auto w-full'>
          <thead>
            <tr>
              <th className='border-2 bg-gray-700 text-psyOrange p-1'>Personal data category</th>
              <th className='border-2 bg-gray-700 text-psyOrange p-1'>Type of personal data</th>
              <th className='border-2 bg-gray-700 text-psyOrange p-1'>Legal ground</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className='border-2 xs:p-0 sm:p-2'>
                <b>Information necessary to subscribe to the Controller’s email subscription form</b>
              </td>
              <td className='border-2 xs:p-0 sm:p-2'>First and last name, Email address</td>
              <td className='border-2 xs:p-0 sm:p-2'>
                Your consent to subscribe to the Controller’s newsletter & future promotions
              </td>
            </tr>
          </tbody>
        </table>
        <TermsHeader title='Purposes of the personal data processing' />
        <p>The Controller processes your personal data for purposes relating to the functioning of the Website:</p>
        <ul>
          <li>
            <b>Email subscription form:</b> to be able to send subscribers occasional emails (once per week on average)
            with free programming ebooks and puzzle worksheets or updates about upcoming publications by the Controller
            and book launch promotions.
          </li>
        </ul>
        <TermsHeader title='Personal data retention periods' />
        <p>
          The Controller retains your personal data for the period necessary to achieve the purposes for which the data
          is collected unless a legal obligation or another legal ground for longer data retention exists.
        </p>
        <table class='table-auto w-full'>
          <thead>
            <tr>
              <th className='border-2 bg-gray-700 text-psyOrange p-1'>Type of personal data</th>
              <th className='border-2 bg-gray-700 text-psyOrange p-1'>Retention period</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className='border-2 xs:p-0 sm:p-2'>
                <b>Data from the email subscription form </b>
              </td>
              <td className='border-2 xs:p-0 sm:p-2'>Until you have withdrawn your consent.</td>
            </tr>
          </tbody>
        </table>
        <TermsHeader title='Sharing of personal data' />
        <p>
          The Controller may, on occasion, pass your personal data to third parties exclusively to process such data on
          Controller’s behalf (e.g.,&nbsp;
          <a href='#'>MailerLite</a> for sending the Controller’s promotional information). The Controller requires any
          such third parties to agree to process your personal information based on our instructions and requirements
          consistent with this Privacy Notice, the UK GDPR and PECR.
        </p>
        <p>
          We do not broker or pass on information gained from your engagement with this Website. Notwithstanding the
          foregoing, the Controller may disclose your personal information to meet his legal obligations, to comply with
          applicable regulations or a valid governmental request.
        </p>
        <TermsHeader title='Data subject rights' />
        <p>
          At any point whilst the Controller holds your personal information, you have a number of rights available to
          you, including the following:
        </p>
        <ul className='list-disc'>
          <li>
            Right of access - you have the right to access a copy of the information that we hold about you and to
            obtain information about how we process it
          </li>
          <li>
            Right of rectification - you have a right to request that we correct data that we hold about you if it
            inaccurate or incomplete
          </li>
          <li>
            Right to be forgotten - in certain circumstances you can ask for the data we hold to be erased from our
            records
          </li>
          <li>
            Right to restriction of processing - where certain conditions apply you have a right to request that we
            restrict the processing of your information
          </li>
          <li>
            Right of portability - you have the right to have the data we hold about you provided to you in an
            electronic format and/or to request that it is transferred to another organisation
          </li>
          <li>
            Right to object - you have the right to object to certain types of processing such as direct marketing
          </li>
          <li>
            Right to object to automated processing including profiling - you also have the right not to be subject to
            the legal effects of automated processing or profiling
          </li>
          <li>
            Right to withdraw consent - to withdraw the consent given by you when this is the data processing legal
            ground
          </li>
        </ul>
        <p>
          You can exercise these rights in the first instance by contacting the Sole Trader (see the contact section on
          this website). Should you wish to do so, the Sole Trader may require you to provide additional information to
          confirm your identity as follows:
        </p>
        <ul className='list-disc'>
          <li>
            One piece of photographic identification (e.g., passport, other national identity card or photocard driving
            license); and/or
          </li>
          <li>One proof of address (e.g., utility bill dated the last 3 months)</li>
        </ul>
        <p>
          If you exercise any of the rights listed above, the Sole Trader will generally reply within a month. We will
          advise you promptly if we require further information in order to fulfil your request.
        </p>
        <TermsHeader title='Updates' />
        <p>
          The Sole Trader may amend this Privacy Notice from time to time to keep it up to date with legal requirements,
          best practice and/or the way the Sole Trader operates its business. If the Sole Trader decides to change the
          Privacy Notice, any updates on this website will be placed.
        </p>
        <TermsHeader title='Contacts' />
        <p>
          In the event that you have any questions, concerns or wish to make a complaint about how your personal data is
          being processed or regarding the Sole Trader’s compliance with this Privacy Notice, you are encouraged to
          contact the Sole Trader via email.
        </p>
        <p>
          <b>Email:</b> chris@psypublishing.com
        </p>
        <p>
          Alternatively you may contact the&nbsp;
          <a href='#'>UK Information Commissioner’s Officer.</a>
        </p>
        <br />
      </div>
    </div>
  );
}

export default Privacy;
