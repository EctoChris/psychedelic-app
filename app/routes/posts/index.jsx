import { Outlet, useLoaderData } from 'remix';

// Loader runs on server: get data / perform action on server and use hook to access on client
export const loader = () => {
  const dummyData = {
    posts: [
      { title: 'post one', id: '1', body: 'this is a test post' },
      { title: 'post two', id: '2', body: 'this is a test post' },
      { title: 'post three ', id: '3', body: 'this is a test post' },
      { title: 'testPost', id: '4', body: 'this is a test post' },
    ],
  };
  console.log('123');
  return dummyData;
};

function Posts() {
  const { posts } = useLoaderData();
  return (
    <div>
      <h1>All posts listed here</h1>
      {posts.map((post, i) => (
        <p key={i}>{post.title}</p>
      ))}
    </div>
  );
}

export default Posts;
