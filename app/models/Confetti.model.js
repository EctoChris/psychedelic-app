const COLORS = [
  [85, 71, 106],
  [174, 61, 99],
  [219, 56, 83],
  [244, 92, 68],
  [248, 182, 70]
]
const PI_2 = 2 * Math.PI

class Confetti {
  constructor(width, height, ctx) {
    this.ctx = ctx
    this.w = width
    this.h = height
    this.style = COLORS[~~this.range(0, 5)]
    this.rgb =
      'rgba(' + this.style[0] + ',' + this.style[1] + ',' + this.style[2]
    this.r = ~~this.range(2, 6)
    this.r2 = 2 * this.r
    this.replace()
  }

  range(a, b) {
    return (b - a) * Math.random() + a
  }

  replace() {
    this.opacity = 0
    this.dop = 0.03 * this.range(1, 4)
    this.x = this.range(-this.r2, this.w - this.r2)
    this.y = this.range(-20, this.h - this.r2)
    this.xmax = this.w - this.r
    this.ymax = this.h - this.r
    this.vx = this.range(0, 2) + 8 * 0.5 - 5
    this.vy = 0.7 * this.r + this.range(-1, 1)
    return this.vy
  }

  draw() {
    let ref
    this.x += this.vx
    this.y += this.vy
    this.opacity += this.dop
    if (this.opacity > 1) {
      this.opacity = 1
      this.dop *= -1
    }
    if (this.opacity < 0 || this.y > this.ymax) {
      this.replace()
    }
    if (!((ref = this.x) > 0 && ref < this.xmax)) {
      this.x = (this.x + this.xmax) % this.xmax
    }
    return this.drawCircle(
      ~~this.x,
      ~~this.y,
      this.r,
      this.rgb + ',' + this.opacity + ')'
    )
  }

  drawCircle(x, y, r, style) {
    this.ctx.beginPath()
    this.ctx.arc(x, y, r, 0, PI_2, false)
    this.ctx.fillStyle = style
    return this.ctx.fill()
  }
}

export default Confetti
