const COLORS = [
  [85, 71, 106],
  [174, 61, 99],
  [219, 56, 83],
  [244, 92, 68],
  [248, 182, 70],
];
const PI_2 = 2 * Math.PI;

class Explosion {
  constructor(width, height, ctx, x, y) {
    this.ctx = ctx;
    this.width = width;
    this.height = height;
    this.style = COLORS[this.range(0, 5)];
    this.rgb = 'rgba(' + this.style[0] + ',' + this.style[1] + ',' + this.style[2];
    console.log('style: ', this.style);
    this.initiateDrawing();
  }

  // ~~ == Math.floor for positive numbers
  range(a, b) {
    return ~~((b - a) * Math.random() + a);
  }

  initiateDrawing() {
    this.x = ~~this.range(10, 200);
    this.y = ~~this.range(10, 300);
    this.opacity = 1;
    this.opacityAdjustment = ~~this.range(1, 10) * 0.03;
    this.r = ~~this.range(2, 6);
    this.rModifier = ~~this.range(0.05, 0.1);
  }

  draw() {
    // let ref;
    // this.x += this.vx;
    // this.y += this.vy;
    this.opacity -= this.opacityAdjustment;
    this.r += this.rModifier;
    console.log(this.opacityAdjustment);
    console.log('opacity: ', this.opacity);
    if (this.opacity < 0) {
      this.initiateDrawing();
    }
    // if (this.opacity < 0 || this.y > this.ymax) {
    //   this.replace();
    // }
    // if (!((ref = this.x) > 0 && ref < this.xmax)) {
    //   this.x = (this.x + this.xmax) % this.xmax;
    // }
    return this.drawCircle(~~this.x, ~~this.y, this.r, this.rgb + ',' + this.opacity + ')');
  }

  drawCircle(x, y, r, style) {
    this.ctx.beginPath();
    this.ctx.arc(x, y, r, 0, PI_2, false);
    this.ctx.fillStyle = style;
    return this.ctx.fill();
  }
}

export default Explosion;
